/// <reference types="Cypress" />

describe('Testing the MovieDB website', () => {
    beforeEach(()=> {
        cy.viewport(1400,800)
        cy.visit('https://www.themoviedb.org/')
        
    })

    it('contains the correct title on top of the window', () =>{
        cy.title().should('contain','The Movie Database (TMDb)')          
    }) 

    it('Allows a valid user to log in', () => {

        cy.get('div.right > .primary > :nth-child(3) > a').click()
        //cy.contains('h2','Login to your Account ')

        cy.url().should('include', 'login')
        //cy.get('.k-form').find('[type="text"]').type('johnnymovie')

        cy.get('.k-form').find('[type="password"]').type('p4ssword')
        cy.get('input[name=username]').type("johnnymovie", {delay:500})
        //cy.get('.k-form').find('[type="password"]').type('p4ssword{enter}')
       // cy.get('#password').type('p4ssword{enter}')
       // cy.get('input[name=password]').type("p4ssword"), { force: true })
     
        //cy.wait(5000)
        cy.get('.k-form').find('[type="submit"]').click()
        //cy.get('.flex > .k-button').click()
          //cy.get('.k-form').submit()
        
     
    })
})

describe('Login in', function () {
    beforeEach(() => {
        cy.viewport(1400, 800)
        cy.visit('https://www.themoviedb.org/login')
    })

    it('allows you to log in', () => {
        //cy.get('[disabled]').click({ force: true })
        // cy.get('#username').type("johnnymovie", { force: true })
        cy.get('#username').type("johnnymovie")
        cy.get('#password').type("p4ssword")
        //cy.get('button[type=Login]').click()
        //cy.get('.flex > .k-button').click({ force: true })
        cy.get('.flex > .k-button').should('contain', 'Login').click()
        cy.get('.k-form').submit()
    })
})


//describe('Can do a simple search', function () {
//    beforeEach(()=> {
 //       cy.get('#search_v4.k-input').type('Glass{enter}')
 //   })
 //   it('requests the results', function () {
 //       cy.url().should('include', 'Glass')
        
       
      
//    })
//})