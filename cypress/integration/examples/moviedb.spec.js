/// <reference types="Cypress" />

describe('Testing the MovieDB website', () => {
    beforeEach(()=> {

        cy.visit('https://www.themoviedb.org/')
        
    })

    it('contains the correct title on top of the window', () =>{
        cy.title().should('contain','The Movie Database (TMDb)')          
    })


})

describe('Can do a simple search', function () {
    beforeEach(()=> {
        cy.get('#search_v4.k-input').type('Glass{enter}')
    })
    it('requests the results', function () {
        cy.url().should('include', 'Glass')
        
       
      
    })
})