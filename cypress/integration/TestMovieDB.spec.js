   /// <reference types="Cypress" />

describe('Does movieDB loads', () => {
	it('loads the website correctly', () => {
        cy.visit('https://www.themoviedb.org/')
        cy.title().should('contain','The Movie Database (TMDb)')
		
    })
})

describe('Test the login function', () => {
    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.visit('https://www.themoviedb.org/')
        cy.get('div.right > .primary > :nth-child(3) > a').click()
        cy.get('.k-form').find('[type="text"]').type('johnnymovie')
        cy.get('.k-form').find('[type="password"]').type('p4ssword')

    })
    it('correct login-incorrectpassword', () => {
        cy.get('.k-form').find('[type="password"]').clear()
        cy.get('.k-form').find('[type="submit"]').click()
        cy.contains('There was a problem')
    })   

    it('incorrect login-correctpassword', () => {
        cy.get('.k-form').find('[type="text"]').clear()
        cy.get('.k-form').find('[type="submit"]').click()
        cy.contains('There was a problem')
    })   

    it('correct login-correctpassword', () => {
        cy.get('.flex > .k-button').should('contain', 'Login').click()
        cy.get('.header > .inner_content').should('contain', 'johnnymovie')
        //cy.get('.k-form').find('[type="submit"]').click()

    })   

})

describe('You can sign up', () => {

    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.visit('https://www.themoviedb.org/')
        cy.get('div.right>.primary>:nth-child(4)>a').click()
            
    })
    it('can sign up a new user', () => {
        const pw = "password123"
        //cy.get('.k-textbox').find('#username').type('randomname123')
        cy.get('#username').type('randomname123').should('have.value','randomname123')
        cy.get('#password').type(pw)
        cy.get('#password_confirm').type(pw)
        cy.get('#email').type("johnny@email.com")


    })
    it('Cancelling your signed should bring you back', () => {
        cy.get('.reset>a').click()
        cy.title('contains','The Movie Database(TMDb')

    })


})


describe('You can do a simple search', () => {

    it('can do a proper search', () => {
        cy.get('#search_v4').type('The Avengers{Enter}')
        cy.get('div.search_results').should('exist')
        cy.get('div.flex>a').contains('The Avengers')
       


            //cy.get('.search-grid-list li').should('exist');
        })

    })

describe('All the top-bar menu works as intended', () => {

    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.visit('https://www.themoviedb.org/')
    })
    it('the discover link works', () => {
       
        cy.get('.left > .primary > :nth-child(2)>:nth-child(1) ').contains('Discover').click()
        cy.get('.main_content').contains('h2','Discover New Movies & TV Shows')
        //cy.contains('')

                   // cy.get('.left > .primary > :nth-child(3) > :nth-child(1)')
                   // cy.get('[href="/discover"]').should('contain','Discover').click()
                   // cy.get('div.left > .primary > :nth-child(2) > a').click()
    })

    it('the movies link should work', () => {
        cy.get('.left > .primary > :nth-child(3) > :nth-child(1)').click()
        cy.contains('Popular Movies')

    })


    it('the TV Show link should work', () => {

        cy.get('.left > .primary > :nth-child(4) > :nth-child(1)').click()
        cy.contains('Popular TV Shows')

    })

    it('the People should work', () => {
        cy.get('.primary > :nth-child(5) > :nth-child(1)').click()
        cy.contains('Popular People')
    })
     
})

describe('Adding a movie without login',()=> {

    it('shows the right error wants to enter a new movie without logging in ', () => {

        cy.get('nav > :nth-child(3) > ul > :nth-child(3) > a').click()
        cy.contains('You need to be logged in!')
    })

})


describe('Adding a TV Show without login', () => {

    it('shows the right error wants to enter a new tv show without logging in ', () => {

        cy.get('nav > :nth-child(3) > ul > :nth-child(4) > a').click()
        cy.contains('You need to be logged in!')
    })

})

describe('Testing the top-right hand-side menu', () => {

    beforeEach(() => {
        cy.viewport(1920, 1080)
        cy.visit('https://www.themoviedb.org/')
    })

    it('will test the Apps link is functional', () => {
        cy.get('.small > :nth-child(1) > a').click()

        cy.contains('Powered by The Movie Database')


    })


    it('will test the Forum link is functional', () => {

        cy.get('.small>:nth-child(2)>a').click()
        cy.contains('Let\'s Chat')

    })


    it('will test the Leaderboard link is functional', () => {
        cy.get('.small>:nth-child(3)>a').click()
        cy.contains('Contribution leaders for the week')

    })


    it('will test the Contribute link is functional', () => {

        cy.get('.small>:nth-child(4)>a').click()
        cy.contains('Help Us By Contributing Missing Data')

    })


    it('will test the API link is functional', () => {
        cy.get('.small>:nth-child(5)>a').click()
        cy.contains('API Overview')


    })


    it('will test the Support link is functional', () => {
        cy.get('.small>:nth-child(6)>a').click()
        cy.contains('The Movie Database Support')
    })

})

describe('the add movie/tv function works', () => {
    it('hover over the plus sign', () => {
        cy.visit('https://www.themoviedb.org/')
        cy.get('.right > .primary > :nth-child(1) > .glyph_wrapper > .glyph > .glyphicons').trigger('.rollover > ::after')
        cy.get('.rollover').contains('Can\'t find a movie or TV show? Login to create it.')

    })

})


//describe('the add movie/tv function works', () => {

//    it('warns me that I need to log on', () => {

//        cy.get('.glyphicons-plus').trigger('mouseover').should('contains','Can\'t find a movie or TV show?')


//    })


//})

